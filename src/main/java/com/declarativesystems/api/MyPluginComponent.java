package com.declarativesystems.api;

public interface MyPluginComponent
{
    String getName();
}