package ut.com.declarativesystems;

import org.junit.Test;
import com.declarativesystems.api.MyPluginComponent;
import com.declarativesystems.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}